import { reminderReducer } from "./reminders";
import { appInternalReducer } from "./app";
import { combineReducers } from "redux";


const appReducer = combineReducers({
  appInternal: appInternalReducer,
  reminder: reminderReducer
});

export default appReducer;
