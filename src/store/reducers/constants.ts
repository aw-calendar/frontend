import { uniqueId } from "lodash"
import { DateComponents, Reminder } from "../../types"
import { AppInternalState, ReminderState } from "./types"

export const DEFAULT_REMINDER_STATE: ReminderState = {
  reminder: null,
  reminders: [
    {
      title: "Title A",
      description: "some description",
      color: "#aaee44",
      dateTime: new Date(),
      id: uniqueId(),
      location: { address: "charcas", placeId: "" }
    },
    {
      title: "Title B",
      description: "some description",
      color: "#eee000",
      dateTime: new Date(),
      id: uniqueId(),
      location: { address: "charcas", placeId: "" }
    },
    {
      title: "Title C",
      description: "some description",
      color: "#e2e2e2",
      dateTime: new Date(),
      id: uniqueId(),
      location: { address: "charcas", placeId: "" }
    },
    {
      title: "Title D",
      description: "some description",
      color: "#ffffff",
      dateTime: new Date(),
      id: uniqueId(),
      location: { address: "charcas", placeId: "" }
    },
    {
      title: "Title",
      description: "some description",
      color: "#000000",
      dateTime: new Date(),
      id: uniqueId(),
      location: { address: "charcas", placeId: "" }
    }
  ],
  selectedReminders: null
}

export const INITIAL_REMINDER = (date: Date = new Date()): Reminder => ({
  title: "",
  description: "",
  dateTime: date,
  location: { address: "charcas", placeId: "" },
  color: "#ffffff"
})

export const DEFAULT_APP_INTERNAL_STATE: AppInternalState = {
  reminderDialog: {
    state: "hide",
    reminder: INITIAL_REMINDER()
  },
  date: {
    now: new Date(),
    currentComponent: DateComponents.month
  }
}
