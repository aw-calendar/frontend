import { AppInternalDateOptions, Reminder, ReminderDialogOptions } from "../../types"
import { ListReminders } from "../actions/types"

export interface ReminderState {
  reminder: Reminder | null
  reminders: Reminder[]
  selectedReminders: null | {
    query: ListReminders
    result: Reminder[]
  }
}

export interface AppInternalState {
  reminderDialog: ReminderDialogOptions
  date: AppInternalDateOptions
}