import { uniqueId } from "lodash";
import { Reminder } from "../../types";
import {
  ADD_REMINDER,
  FIND_REMINDER,
  LIST_REMINDERS,
  REMOVE_REMINDER,
  UPDATE_REMINDER,
} from "../actions/constants";
import {
  AddReminder,
  RemoveReminder,
  ListReminders,
  UpdateReminder,
  FindReminder,
  AnyReminderAction,
} from "../actions/types";
import { DEFAULT_REMINDER_STATE } from "./constants";
import { ReminderState } from "./types";

const optionalMergeFactory = (id: string, params: Reminder) => (
  source: Reminder | null
) => {
  if (source && source.id === id) {
    return { ...source, ...params } as Reminder;
  }
  return source as Reminder;
};

export const listReminders = (state: ReminderState, action: ListReminders): ReminderState => {
  return {
    ...state,
    selectedReminders: {
      query: action,
      result: state.reminders.filter(
        (reminder) =>
          reminder.dateTime >= action.query.from &&
          reminder.dateTime <= action.query.to
      ),
    },
  };
};

export const addReminder = (state: ReminderState, action: AddReminder): ReminderState => {
  const resultingState = {
    ...state,
    reminders: [...state.reminders, { ...action.reminder, id: action.reminder.id || uniqueId() }],
  };

  if (state.selectedReminders && state.selectedReminders.query)
    return listReminders(resultingState, state.selectedReminders.query);

  return resultingState;
};

export const removeReminder = (
  state: ReminderState,
  action: RemoveReminder
): ReminderState => {
  const resultingState = {
    ...state,
    reminders: state.reminders.filter((reminder) => reminder.id !== action.id),
  };

  if (state.selectedReminders && state.selectedReminders.query)
    return listReminders(resultingState, state.selectedReminders.query);

  return resultingState;
};

export const updateReminder = (
  state: ReminderState,
  action: UpdateReminder
): ReminderState => {
  const optionalMerge = optionalMergeFactory(
    action.payload.id,
    action.payload.params
  );

  const resultingState = {
    ...state,
    reminder: optionalMerge(state.reminder),
    reminders: state.reminders.map(optionalMerge),
  };

  if (state.selectedReminders && state.selectedReminders.query)
    return listReminders(resultingState, state.selectedReminders.query);

  return resultingState;
};

export const findReminder = (state: ReminderState, action: FindReminder): ReminderState => {
  return {
    ...state,
    reminder: state.reminders.find(({ id }) => id === action.id),
  } as ReminderState;
};

export const reminderReducer = (
  state: ReminderState = DEFAULT_REMINDER_STATE,
  action: AnyReminderAction
): ReminderState => {
  switch (action.type) {
    case ADD_REMINDER:
      return addReminder(state, action);
    case LIST_REMINDERS:
      return listReminders(state, action);
    case REMOVE_REMINDER:
      return removeReminder(state, action);
    case FIND_REMINDER:
      return findReminder(state, action);
    case UPDATE_REMINDER:
      return updateReminder(state, action);
    default:
      return state;
  }
};
