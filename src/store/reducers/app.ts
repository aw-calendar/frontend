
import { REMINDER_DIALOG, SET_DATE } from "../actions/constants";
import { AppInternalReminderDialog, AnyAppInternalAction, AppInternalSetDateAction } from "../actions/types";
import { DEFAULT_APP_INTERNAL_STATE } from "./constants";
import { AppInternalState } from "./types";

export const reminderDialogReducer = (state: AppInternalState, action: AppInternalReminderDialog): AppInternalState => {
  return {
    ...state,
    reminderDialog: {
      ...state.reminderDialog,
      ...action.payload
    }
  }
};

export const setOnDate = (state: AppInternalState, action: AppInternalSetDateAction): AppInternalState => {
  return {
    ...state,
    date: {
      ...state.date,
      ...action.payload
    }
  }
};

export const appInternalReducer = (state: AppInternalState = DEFAULT_APP_INTERNAL_STATE, action: AnyAppInternalAction): AppInternalState => {
  switch (action.type) {
    case REMINDER_DIALOG:
      return reminderDialogReducer(state, action);
    case SET_DATE:
      return setOnDate(state, action);
    default:
      return state;
  }
}

