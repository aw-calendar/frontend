import { createStore, Store, applyMiddleware  } from "redux"
import ReduxThunk from "redux-thunk"
import type { AppInternalState, ReminderState } from "./reducers/types"
import rootReducer from "./reducers"
import { AnyAppInternalAction, AnyReminderAction } from "./actions/types"

export type AppState = { appInternal: AppInternalState; reminder: ReminderState }
export type AnyAppAction = AnyAppInternalAction | AnyReminderAction

const store: Store<AppState, AnyAppAction> = createStore(rootReducer, applyMiddleware(ReduxThunk))

export default store
