import { DateComponents, Reminder, ReminderDialogOptions, ReminderQuery, ReminderUpdater } from "../../types"
import {
  ADD_REMINDER,
  FIND_REMINDER,
  LIST_REMINDERS,
  REMINDER_DIALOG,
  REMOVE_REMINDER,
  SET_DATE,
  UPDATE_REMINDER
} from "./constants"

export interface AddReminder {
  type: typeof ADD_REMINDER
  reminder: Reminder
}

export interface RemoveReminder {
  type: typeof REMOVE_REMINDER
  id: string
}

export interface FindReminder {
  type: typeof FIND_REMINDER
  id: string
}

export interface ListReminders {
  type: typeof LIST_REMINDERS
  query: ReminderQuery
}

export interface UpdateReminder {
  type: typeof UPDATE_REMINDER
  payload: ReminderUpdater
}

export interface AppInternalReminderDialog {
  type: typeof REMINDER_DIALOG
  payload: ReminderDialogOptions
}



export interface AppInternalSetDateAction {
  type: typeof SET_DATE
  payload: { now?: Date; currentComponent?: DateComponents }
}

export type AnyReminderAction =
  | UpdateReminder
  | AddReminder
  | FindReminder
  | RemoveReminder
  | ListReminders

export type AnyAppInternalAction = AppInternalReminderDialog | AppInternalSetDateAction
