import { DateComponents, ReminderDialogOptions } from "../../types";
import { REMINDER_DIALOG, SET_DATE } from "./constants";
import { AppInternalReminderDialog, AppInternalSetDateAction } from "./types";

export const reminderDialog = (payload: ReminderDialogOptions): AppInternalReminderDialog => ({ type: REMINDER_DIALOG, payload });
export const setOnDate = (payload: { now?: Date, currentComponent?: DateComponents }): AppInternalSetDateAction => ({ type: SET_DATE, payload });