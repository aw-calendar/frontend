import { ThunkAction } from "redux-thunk"
import { getWeatherCondition } from "../../services/weatherService"
import { geocodeByPlaceId } from "react-google-places-autocomplete"
import { Reminder, ReminderQuery, ReminderUpdater } from "../../types"
import { ReminderState } from "../reducers/types"
import {
  ADD_REMINDER,
  REMOVE_REMINDER,
  LIST_REMINDERS,
  UPDATE_REMINDER,
  FIND_REMINDER
} from "./constants"
import { AddReminder, RemoveReminder, ListReminders, UpdateReminder, FindReminder } from "./types"

export const addReminder = (reminder: Reminder): AddReminder => ({ type: ADD_REMINDER, reminder })
export const removeReminder = (id: string): RemoveReminder => ({ type: REMOVE_REMINDER, id })
export const listReminders = (query: ReminderQuery): ListReminders => ({
  type: LIST_REMINDERS,
  query
})
export const updateReminder = (payload: ReminderUpdater): UpdateReminder => ({
  type: UPDATE_REMINDER,
  payload
})
export const findReminder = (id: string): FindReminder => ({ type: FIND_REMINDER, id })

export const updateReminderThunk = (
  payload: ReminderUpdater
): ThunkAction<void, ReminderState, unknown, UpdateReminder> => async (dispatch) => {
  const { params } = payload

  if (params && params.location && params.location.placeId) {
    const [location] = await geocodeByPlaceId(params.location.placeId)
    const coordinates = location.geometry.location.toJSON()

    params.contidion = await getWeatherCondition(coordinates, params.dateTime)
  }

  dispatch(updateReminder({ ...payload, params }))
}

export const addReminderThunk = (
  reminder: Reminder
): ThunkAction<void, ReminderState, unknown, AddReminder> => {
  return async (dispatch) => {
    
    if (!reminder || !reminder.location || !reminder.location.placeId) {
      return
    }

    const [location] = await geocodeByPlaceId(reminder.location.placeId)
    const coordinates = location.geometry.location.toJSON()

    reminder.contidion = await getWeatherCondition(coordinates, reminder.dateTime)

    console.log(reminder)

    dispatch(addReminder(reminder))
  }
}
