import { useTranslation } from "react-i18next"
import moment from "moment"
import "moment/locale/es";

const useLocaleMoment = (lng?: string): typeof moment => {
  const { i18n } = useTranslation()

  moment.locale(lng || i18n.options.lng)

  return moment
}

export default useLocaleMoment;