import { RefObject } from "react"
import { useState, useLayoutEffect, useRef } from "react"

function useDimensions(): [
  { height: number; width: number },
  RefObject<HTMLDivElement> | undefined
] {
  const targetRef = useRef<HTMLDivElement>()

  const [dimensions, setDimensions] = useState({ width: 0, height: 0 })

  const handleChange = () => {
    if (
      targetRef.current &&
      (targetRef.current.offsetWidth !== dimensions.width ||
        targetRef.current.offsetHeight !== dimensions.height)
    ) {
      setDimensions({
        width: targetRef.current.offsetWidth,
        height: targetRef.current.offsetHeight
      })
    }
  }

  useLayoutEffect(() => {
    handleChange()
    window.addEventListener("resize", handleChange)

    return () => window.removeEventListener("resize", handleChange)
  }, [])

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return [dimensions, targetRef as any]
}

export default useDimensions
