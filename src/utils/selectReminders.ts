import { Reminder } from "../types"

export const selectReminders = (reminders: Reminder[], from: Date, to: Date): Reminder[] => {
  return reminders.filter((reminder) => reminder.dateTime >= from && reminder.dateTime <= to).sort((a, b) => a.dateTime.getTime() - b.dateTime.getTime())
}