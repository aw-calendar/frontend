import React from "react"
import { Provider } from "react-redux"
import ReactDOM from "react-dom"
import "./index.css"
import "./i18n"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import store from "./store"
import { ThemeProvider } from "styled-components"
import { createMuiTheme, MuiThemeProvider, StylesProvider } from "@material-ui/core"

const theme = createMuiTheme({})

ReactDOM.render(
  <Provider store={store}>
    <StylesProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
      </MuiThemeProvider>
    </StylesProvider>
  </Provider>,
  document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
