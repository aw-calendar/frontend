import React from "react"
import { Container, Row } from "react-grid-system"
import styled from "styled-components"
import Years from "./components/Years"
import Months from "./components/Months"
import { connect } from "react-redux"
import { AppState } from "./store"
import { AnyAction } from "redux"
import ReminderModal from "./components/Reminder/modal"
import Navbar from "./components/Navbar"
import { DateComponents } from "./types"
import { AppInternalState } from "./store/reducers/types"
import Day from "./components/Day"

const StyledNavbar = styled(Navbar)`
  height: 50px;
`

const StyledContainer = styled(Container)`
  overflow-y: auto;
  max-height: calc(100vh - 50px);
`

const App: React.FunctionComponent<{ appInternal: AppInternalState;  dispatch: (action: AnyAction) => void }> = ({
  appInternal
}) => {
  return (
    <>
      {appInternal.reminderDialog.state !== "hide" && <ReminderModal />}
      <StyledNavbar/>
      <StyledContainer fluid>
        <Row>
          {appInternal.date.currentComponent === DateComponents.year && <Years />}
          {appInternal.date.currentComponent === DateComponents.month && <Months />}
          {appInternal.date.currentComponent === DateComponents.date && <Day />}
        </Row>
      </StyledContainer>
    </>
  )
}

export default connect(
  (state: AppState) => ({ appInternal: state.appInternal }),
  (dispatch: (action: AnyAction) => void) => ({ dispatch })
)(App)
