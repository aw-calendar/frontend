import { TextField, TextFieldProps, Theme } from "@material-ui/core"
import { noop } from "lodash"
import React from "react"
import ColorPicker, { useColor } from "react-color-palette"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { Container, Row } from "react-grid-system"

const styled: ThemedStyledInterface<Theme> = styledRef;

const Box = styled.div<{ width: number; height: number; color: string }>`
  min-width: ${(props) => props.width}px;
  min-height: ${(props) => props.height}px;
  background-color: ${(props) => props.color};
  border: 1px solid ${(props) => props.theme?.palette.grey[900] || "#000000"};
`

const ColorPickerField: React.FunctionComponent<
  TextFieldProps & {
    value: string
    disabled?: boolean
  }
> = ({ value, onChange = noop, disabled = false, ...props }) => {
  const [color, setColor] = useColor("hex", value || "#ffffff")

  if (disabled) {
    return (
      <>
        <Container>
          <Row>
            <TextField value={value} disabled={disabled} {...props} />
          </Row>
          <Row justify="center" align="center">
            <Box width={200} height={10} color={value} />
          </Row>
        </Container>
      </>
    )
  }

  return (
    <ColorPicker
      width={400}
      height={100}
      color={color}
      onChange={(val) => {
        setColor(val)
        onChange(val.hex)
      }}
    />
  )
}

export default ColorPickerField
