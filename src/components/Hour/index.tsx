import { Button, Theme, Typography } from "@material-ui/core"
import { AddCircle } from "@material-ui/icons"
import { uniqueId } from "lodash"
import React from "react"
import { Container, Row } from "react-grid-system"
import { connect } from "react-redux"
import { Dispatch } from "redux"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { AppState } from "../../store"
import { reminderDialog } from "../../store/actions/app"
import { INITIAL_REMINDER } from "../../store/reducers/constants"
import { DateComponents, Reminder } from "../../types"
import { selectReminders } from "../../utils/selectReminders"
import useLocaleMoment from "../../utils/useMoment"
import ReminderTip from "../Reminder/tip"

const styled: ThemedStyledInterface<Theme> = styledRef;

const Divider = styled.div`
  border-bottom: 1px solid ${props => props.theme?.palette.grey[800] || "#fff"};
  background-color: ${props => props.theme?.palette.grey[600] || "#dadada"};
  height: 1px;
  margin: ${props => `${props?.theme?.spacing(0.5)}px` || "0.5em"} 0px ${props => `${props?.theme?.spacing(1.5)}px` || "1.5em"};
  min-width: 100%;
`

const DividerColor = styled.span<{ color?: string }>`
  display: block;
  width: 50px;
  height: 1px;
  background-color: ${(props) => props.color || "#f60"};
`

const StyledRow = styled(Row)`
  min-width: 90vw;
`

const Hours: React.FunctionComponent<{
  reminders: Reminder[]
  date?: Date
  step: number
  internalAppDate: Date;
  dispatch: Dispatch;
}> = ({ reminders, date, step, internalAppDate, dispatch }) => {
  const dateRef = date || internalAppDate
  const moment = useLocaleMoment()
  const now = moment(date)
  const selected = selectReminders(reminders, dateRef, now.clone().add(step, "minutes").toDate())

  const disabled: boolean = moment().startOf(DateComponents.hour).isAfter(now)

  const openCreate = () => {
    dispatch(reminderDialog({ state: "show-create", reminder: INITIAL_REMINDER(date) }))
  }

  return (
    <>
      <Container fluid>
        <StyledRow>
          <Typography>{now.format("HH:mm")}</Typography>
          <Button onClick={openCreate} disabled={disabled}>
            <AddCircle color={disabled ? "disabled" : "primary"} />
          </Button>
        </StyledRow>
        <Divider>
          <DividerColor />
        </Divider>
        {selected.map((reminder) => (
          <Row key={uniqueId()}>
            <ReminderTip reminder={reminder} mini={false} />
          </Row>
        ))}
      </Container>
    </>
  )
}

export default connect(
  (state: AppState) => ({
    internalAppDate: state.appInternal.date.now,
    reminders: state.reminder.reminders
  }),
  (dispatch: Dispatch) => ({ dispatch })
)(Hours)
