import React from "react"
import { connect } from "react-redux"
import { Reminder, ReminderDialogState } from "../../types"
import { Dispatch } from "redux"
import { reminderDialog } from "../../store/actions/app"
import styled from "styled-components"

const ClickableDiv = styled.div`
  cursor: pointer;
`;

const ReminderButton: React.FunctionComponent<
  Record<string, unknown> &
  {
    dispatch: Dispatch
    reminder?: Reminder
    state: ReminderDialogState;
    render?: React.ReactElement;
    as: React.FunctionComponent<unknown>
  }
> = ({ state, reminder, dispatch, render = <></>, as = ClickableDiv, ...props }) => {
  const Class = as;
  return <Class {...props} onClick={() => dispatch(reminderDialog({ state, reminder }))}>{render}</Class>
}

export default connect(
  () => ({}),
  (dispatch: Dispatch) => ({ dispatch })
)(ReminderButton)
