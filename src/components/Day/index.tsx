import { range, toLower, uniqueId } from "lodash"
import styledRef, { ThemedStyledInterface } from "styled-components"
import React from "react"
import { useTranslation } from "react-i18next"
import { Container, Row } from "react-grid-system"
import { AppState } from "../../store"
import { Dispatch } from "redux"
import { connect } from "react-redux"
import useLocaleMoment from "../../utils/useMoment"
import Hour from "../Hour"
import { Theme } from "@material-ui/core"

const styled: ThemedStyledInterface<Theme> = styledRef;

const MonthContainer = styled(Container)`
  &:focus {
    outline: none;
  }
`

const MonthOutline = styled.div`
  ${MonthContainer}:focus & {
    outline: 4px solid ${props => props?.theme?.palette.grey[600] || "#dab08c"};
  }
`

const MonthTitle = styled.h1`
  height: 52px;
  margin: 16px;
  text-align: center;
  font-size: ${props => props?.theme?.typography.h3.fontSize || "3.2em"};
  cursor: pointer;
`

const Months: React.FunctionComponent<{
  appInternalDate: Date
  date?: Date
}> = ({ appInternalDate, date }) => {
  const { t } = useTranslation()
  const moment = useLocaleMoment()
  const now = moment(date || appInternalDate).startOf("date")


  const hours = range(0, 24)

  return (
    <>
      <MonthContainer>
        <MonthOutline />
        <Row>
          <MonthTitle>
            {t(`monthView.${toLower(now.format("dddd"))}`)}
          </MonthTitle>
        </Row>
        {hours.map((hour) => (
          <Row key={uniqueId()}>
            <Hour date={now.clone().set("hour", hour).startOf("hour").toDate()} step={59} />
          </Row>
        ))}
      </MonthContainer>
    </>
  )
}

export default connect(
  (state: AppState) => ({ appInternalDate: state.appInternal.date.now }),
  (dispatch: Dispatch) => ({ dispatch })
)(Months)
