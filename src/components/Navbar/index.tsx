import React from "react"
import { Col, Container, Row } from "react-grid-system"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { useTranslation } from "react-i18next"
import DateComponentPicker from "../DateComponentPicker"
import ReminderButton from "../ReminderButton"
import AppDatePicker from "../AppDatePicker"
import LangPicker from "../LangPicker"
import { Button, Theme } from "@material-ui/core"

const styled: ThemedStyledInterface<Theme> = styledRef;

const StickyContainer = styled.div`
  padding: 5px;
  z-index: 1200;
  background-color: ${(props) => props.theme?.palette.common.white || "#ececec"};
  border-bottom: 2px solid ${(props) => props.theme?.palette.info.light || "#ececec"};
`


const Navbar: React.FunctionComponent<React.HTMLAttributes<HTMLDivElement>> = (props) => {
  const { t } = useTranslation()

  return (
    <StickyContainer {...props}>
      <Container fluid>
        <Row justify="between" align="center">
          <Col xs="content">
            <DateComponentPicker />
          </Col>
          <Col>
            <AppDatePicker />
          </Col>
          <Col xs="content">
            <ReminderButton
              as={Button}
              variant="outlined"
              color="primary"
              state={"show-create"}
              render={<>{t("navbar.addReminderLabel")}</>}
            ></ReminderButton>
          </Col>
          <Col xs="content">
            <LangPicker />
          </Col>
        </Row>
      </Container>
    </StickyContainer>
  )
}

export default Navbar
