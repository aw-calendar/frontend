import { toLower, uniqueId } from "lodash"
import styledRef, { ThemedStyledInterface } from "styled-components"
import React from "react"
import { useTranslation } from "react-i18next"
import { Container, Row } from "react-grid-system"
import Date from "../Date"
import { AppState } from "../../store"
import { Dispatch } from "redux"
import { connect } from "react-redux"
import { setOnDate } from "../../store/actions/app"
import { DateComponents } from "../../types"
import useLocaleMoment from "../../utils/useMoment"
import { Moment } from "moment"
import { Theme } from "@material-ui/core"

const styled: ThemedStyledInterface<Theme> = styledRef;

const MonthContainer = styled(Container)`
  &:focus {
    outline: none;
  }
`

const MonthOutline = styled.div`
  ${MonthContainer}:focus & {
    outline: 4px solid ${props => props.theme?.palette.info.main || "#dab08c"};
  }
`

const MonthTh = styled.th`
  width: ${() => 100 / 7}%;
  height: 32px;
  padding: 0 0 12px;
  text-align: center;

  &::after {
    content: "";
    display: block;
    width: ${() => 100}%;
    height: 20px;
    background-color: ${props => props.theme?.palette.success.light || "#acc2c2"};
    transition: opacity 0.4s;
  }
`

const MonthTd = styled.th`
  width: ${() => 100 / 7}%;
  padding-top: ${() => 100 / 7}%;
  vertical-align: top;
  border: 3px solid ${props => props.theme?.palette.info.light || "#dfe7e7"};
  border-radius: 5px;
`

const MonthTdContent = styled.div`
  position: absolute;
  margin-top: -${() => 100 / 7}%;
  padding: ${props => `${props.theme?.spacing(1)}px`};
  margin-left: auto;
  margin-right: auto;
  width: ${() => 100 / 8}%;
`

const MonthTitle = styled.h1`
  height: 52px;
  margin: 16px;
  text-align: center;
  font-size: ${props => `${props.theme?.spacing(3.2)}px`};
  cursor: pointer;
`

const MonthTable = styled.table`
  width: 100%;
  margin: 16px;
  table-layout: fixed;
  border-collapse: separate;
  border-spacing: 4px;
`

const StyledDate = styled(Date)``

const Months: React.FunctionComponent<{ appInternalDate: Date; dispatch: Dispatch; date?: Date }> = ({
  appInternalDate,
  date,
  dispatch,
}) => {
  const { t } = useTranslation();
  const now: Date = date || appInternalDate
  const moment = useLocaleMoment()
  const nowMoment = moment(now)
  const startWeek = nowMoment.clone().startOf("month").week()
  const endWeek =
    nowMoment.get("month") === 11
      ? nowMoment.clone().weeksInYear() + 1
      : nowMoment.clone().endOf("month").week()

  let calendar: Array<{ week: number; days: Array<Moment | null> }> = []
  for (let week = startWeek; week <= endWeek; week++) {
    calendar.push({
      week: week,
      days: Array(7)
        .fill(0)
        .map((_, i) => {
          const day = nowMoment.clone().week(week).startOf("week").clone().add(i, "day")

          return day.isSame(nowMoment.clone(), "month") ? day : null
        })
    })
  }

  calendar = calendar.filter((entry) => entry.days.filter((day) => !!day).length)

  return (
    <>
      <MonthContainer>
        <MonthOutline />
        <Row>
          <MonthTitle
            onClick={() => dispatch(setOnDate({ now, currentComponent: DateComponents.month }))}
          >
            {t(`date.${toLower(nowMoment.clone().format("MMMM"))}`)}
          </MonthTitle>
        </Row>
        <Row>
          <MonthTable>
            <thead>
              <tr>
                <MonthTh>{t("monthView.sunday")}</MonthTh>
                <MonthTh>{t("monthView.monday")}</MonthTh>
                <MonthTh>{t("monthView.tuesday")}</MonthTh>
                <MonthTh>{t("monthView.wednesday")}</MonthTh>
                <MonthTh>{t("monthView.thursday")}</MonthTh>
                <MonthTh>{t("monthView.friday")}</MonthTh>
                <MonthTh>{t("monthView.sat")}</MonthTh>
              </tr>
            </thead>
            <tbody>
              {calendar.map((item) => (
                <tr key={uniqueId()}>
                  {item.days.map((day) => (
                    <MonthTd key={uniqueId()}>
                      <MonthTdContent>{day && <StyledDate date={day.toDate()} />}</MonthTdContent>
                    </MonthTd>
                  ))}
                </tr>
              ))}
            </tbody>
          </MonthTable>
        </Row>
      </MonthContainer>
    </>
  )
}

export default connect(
    (state: AppState) => ({ appInternalDate: state.appInternal.date.now }),
    (dispatch: Dispatch) => ({ dispatch })
  )(Months)
