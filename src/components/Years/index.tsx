import { range, uniqueId } from "lodash"
import React from "react"
import { Col, Container, Row } from "react-grid-system"
import styled from "styled-components"
import Months from "../Months"
import { connect } from "react-redux"
import { AppState } from "../../store"
import useLocaleMoment from "../../utils/useMoment"

const YearContainer = styled(Container)`
  text-decoration: none;
`

const YearMonths = styled(Row)`
  margin: 0
  padding: 0;
  list-style: none;
`

const YearMonthItem = styled(Col)`
  max-width: 25%;
`

const Years: React.FunctionComponent<{ appInternalDate: Date, date?: Date }> = ({ appInternalDate, date }) => {
  const now: Date = date || appInternalDate
  const moment = useLocaleMoment()
  return (
    <>
      <YearContainer>
        <YearMonths>
          {range(12).map((crr) => {
            const currentDate = moment(now).set("month", crr).startOf("month").toDate()
            return (
              <YearMonthItem key={uniqueId()} xs={6}>
                <Months date={currentDate} />
              </YearMonthItem>
            )
          })}
        </YearMonths>
      </YearContainer>
    </>
  )
}

export default connect((state: AppState) => ({ appInternalDate: state.appInternal.date.now }))(Years)
