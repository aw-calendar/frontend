import React from "react"
import { resources, schema } from "../../i18n";
import { useTranslation } from "react-i18next";
import { toLower, toUpper, uniqueId } from "lodash";
import { InputLabel, Select, MenuItem } from "@material-ui/core";

const LangPicker: React.FunctionComponent<unknown> = () => {
  const { t, i18n } = useTranslation();
  
  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(toLower(lng));
  }

  const langs = Object.keys(resources).map(toUpper);

  return (
    <>
      <InputLabel id="demo-simple-select-label">{t("langPicker.label")}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        defaultValue={toUpper(schema.lng)}
        onChange={(e) =>
          changeLanguage(e.target.value as string )
        }
      >
        {langs.map((option) => (
          <MenuItem key={uniqueId()} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </>
  )
}

export default LangPicker