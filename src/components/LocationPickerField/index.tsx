import { TextField, TextFieldProps } from "@material-ui/core";
import { noop } from "lodash";
import React, { useRef, useState } from "react";
import PlacesAutocomplete from "react-google-places-autocomplete"
import constants from "../../config/constants";
import { Place } from "../../types";

const LocationPickerField: React.FunctionComponent<TextFieldProps & {
  value?: Place;
  disabled?: boolean;
  onLocationChange?: (val: Place) => void
}> = ({ value: source, onLocationChange: onChange = noop, disabled = false, ...props }) => {

  const [value, setValue] = useState({ label: source?.address || "", value: { place_id: source?.placeId, description: source?.address || "" } });
  const ref = useRef(null);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleChange = (val: any) => {
    setValue(val);
    onChange({ address: val?.label, placeId: val?.value?.place_id });
  }

  if (disabled)
    return <TextField {...props} value={value?.label} disabled={true} />

  return (
    <PlacesAutocomplete
      apiKey={constants.GOOGLE_MAPS_API_KEY}
      selectProps={{
        ref,
        styles: { container: (provider) => ({ ...provider, minWidth: "100%" }) },
        defaultInputValue: source?.address || "",
        onChange: handleChange,
        onInputChange: val => setValue({ ...value, label: val, value: { ...value.value, description: val }  }),
      }}
    />
  )
}

export default LocationPickerField;