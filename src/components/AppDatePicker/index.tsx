import { InputLabel, MenuItem, Select, TextField, Theme } from "@material-ui/core"
import { toLower, uniqueId } from "lodash"
import React, { useEffect, useState } from "react"
import { Container, Row } from "react-grid-system"
import { useTranslation } from "react-i18next"
import { connect } from "react-redux"
import { Dispatch } from "redux"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { AppState } from "../../store"
import { setOnDate } from "../../store/actions/app"
import { AppInternalState } from "../../store/reducers/types"
import { DateComponents } from "../../types"
import useDebounce from "../../utils/useDebounce"
import useLocaleMoment from "../../utils/useMoment"

const styled: ThemedStyledInterface<Theme> = styledRef;

const DateContainer = styled(Row)`
  text-decoration: none;
`

const AppDatePicker: React.FunctionComponent<AppInternalState & { dispatch: Dispatch }> = ({
  dispatch,
  date: crrDate
}) => {
  const { now, currentComponent } = crrDate
  const { t } = useTranslation()
  const moment = useLocaleMoment()
  const date = moment(now)

  const [value, setValue] = useState({
    year: date.get(DateComponents.year),
    month: date.get(DateComponents.month),
    date: date.get(DateComponents.date)
  })
  const debouncedValue = useDebounce(value, 500)

  useEffect(() => {
    dispatch(
      setOnDate({
        now: date
          .clone()
          .set(DateComponents.month, debouncedValue.month)
          .set(DateComponents.year, debouncedValue.year)
          .set(DateComponents.date, debouncedValue.date)
          .toDate()
      })
    )
  }, [debouncedValue])

  const handleChange = (comp: DateComponents, val: number) => {
    setValue({ ...value, [comp]: val })
  }

  const months = moment.months()

  const compOrder = [DateComponents.year, DateComponents.month, DateComponents.date].indexOf(
    currentComponent
  )

  return (
    <>
      <Container>
        <DateContainer align="end" justify="around">
          {compOrder >= 0 && (
            <TextField
              name="year"
              label={t("dateComponents.year")}
              type="number"
              value={value[DateComponents.year]}
              onChange={(e) => handleChange(DateComponents.year, Number(e.target.value))}
            />
          )}
          {compOrder >= 1 && (
            <div>
              <InputLabel id="month-select">{t("dateComponents.month")}</InputLabel>
              <Select
                labelId="month-select"
                value={value[DateComponents.month]}
                onChange={(e) => handleChange(DateComponents.month, Number(e.target.value))}
              >
                {months.map((option, i) => (
                  <MenuItem key={uniqueId()} value={i}>
                    {t(`date.${toLower(option)}`)}
                  </MenuItem>
                ))}
              </Select>
            </div>
          )}
          {compOrder >= 2 && (
            <TextField
              name="date"
              label={t("dateComponents.day")}
              type="number"
              value={value[DateComponents.date]}
              onChange={(e) => handleChange(DateComponents.date, Number(e.target.value))}
            />
          )}
        </DateContainer>
      </Container>
    </>
  )
}

export default connect(
  (state: AppState) => ({ ...state.appInternal }),
  (dispatch: Dispatch) => ({ dispatch })
)(AppDatePicker)
