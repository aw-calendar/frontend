import { range, uniqueId } from "lodash"
import React from "react"
import { Container, Row } from "react-grid-system"
import { DateComponents, Reminder } from "../../types"
import ReminderTip from "../Reminder/tip"
import styledRef, { ThemedStyledInterface } from "styled-components"
import useLocaleMoment from "../../utils/useMoment"
import useDimensions from "../../utils/useDimensions"
import { Button, Theme, Typography } from "@material-ui/core"
import { AddCircle } from "@material-ui/icons"
import { connect } from "react-redux"
import { reminderDialog, setOnDate } from "../../store/actions/app"
import { INITIAL_REMINDER } from "../../store/reducers/constants"
import { Dispatch } from "redux"
import { AppState } from "../../store"
import { selectReminders } from "../../utils/selectReminders"

const styled: ThemedStyledInterface<Theme> = styledRef;

const FlexRow = styled.div<{ flexDirection: "row" | "column" }>`
  padding-top: ${props => `${props?.theme?.spacing(1)}px` || "1em"};
  display: flex;
  flex-direction: ${(props) => props.flexDirection};
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`

const SquareContainer = styled.div<{ height: number }>`
  height: ${(props) => props.height}px;
  max-height: ${(props) => props.height}px;
  overflow-y: auto;
`

const Date: React.FunctionComponent<{ reminders: Reminder[]; date: Date; dispatch: Dispatch }> = ({
  date,
  reminders,
  dispatch
}) => {
  const moment = useLocaleMoment()
  const nowRef = moment(date)
  const now = moment()

  const components = [DateComponents.year, DateComponents.month, DateComponents.date]

  components.forEach((comp: DateComponents) => {
    now.set(comp, nowRef.get(comp))
  })

  const selected = selectReminders(
    reminders || [],
    now.clone().startOf("date").toDate(),
    now.clone().endOf("date").toDate()
  )

  const [dimensions, ref] = useDimensions()
  const mini = dimensions.width < 125

  console.log("re render date view")

  const openCreate = () => {
    dispatch(reminderDialog({ state: "show-create", reminder: INITIAL_REMINDER(date) }))
  }

  const disabled: boolean = moment().startOf(DateComponents.date).isAfter(now)

  return (
    <SquareContainer ref={ref} height={dimensions.width}>
      <Container>
        <Row justify="between" align="center">
          <Typography
            onClick={() =>
              dispatch(
                setOnDate({
                  now: now.clone().startOf("date").toDate(),
                  currentComponent: DateComponents.date
                })
              )
            }
          >
            {now.format("DD") || ""}
          </Typography>
          <Button onClick={openCreate} disabled={disabled}>
            <AddCircle color={disabled ? "disabled" : "primary"} />
          </Button>
        </Row>
        <FlexRow flexDirection={mini ? "row" : "column"}>
          {range(Math.min(selected.length, 5)).map((crr) => (
            <ReminderTip key={uniqueId()} mini={mini} reminder={selected[crr]} />
          ))}
        </FlexRow>
      </Container>
    </SquareContainer>
  )
}

export default connect(
  (state: AppState) => ({ reminders: state.reminder.reminders }),
  (dispatch: Dispatch) => ({ dispatch })
)(Date)
