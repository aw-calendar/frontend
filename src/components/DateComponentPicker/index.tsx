import React from "react"
import { InputLabel, Select, MenuItem } from "@material-ui/core"
import { uniqueId } from "lodash"
import { connect } from "react-redux"
import { AppInternalDateOptions, DateComponents } from "../../types"
import { AppState } from "../../store"
import { Dispatch } from "redux"
import { setOnDate } from "../../store/actions/app"
import { useTranslation } from "react-i18next"

const DateComponentPicker: React.FunctionComponent<{
  date: AppInternalDateOptions
  dispatch: Dispatch
}> = ({ date, dispatch }) => {
  const { t } = useTranslation()

  const availableViews = [DateComponents.year, DateComponents.month, DateComponents.date]

  return (
    <>
      <InputLabel id="date-component-select-label">{t("dateComponents.label")}</InputLabel>
      <Select
        labelId="date-component-select-label"
        id="date-component-select"
        value={date.currentComponent}
        onChange={(e) =>
          dispatch(setOnDate({ currentComponent: e.target.value as DateComponents }))
        }
      >
        {availableViews.map((option) => (
          <MenuItem key={uniqueId()} value={option}>
            {t(`dateComponents.${option}`)}
          </MenuItem>
        ))}
      </Select>
    </>
  )
}

export default connect(
  (state: AppState) => ({ date: state.appInternal.date }),
  (dispatch: Dispatch) => ({ dispatch })
)(DateComponentPicker)
