import { TFunction } from "react-i18next"
import * as Yup from "yup"

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const ReminderSchema = (t: TFunction) =>
  Yup.object().shape({
    title: Yup.string()
      .min(2, t("reminder.modal.title.min", { min: 2 }))
      .max(50, t("reminder.modal.title.max", { max: 50 }))
      .required(t("reminder.modal.title.required")),
    description: Yup.string()
      .min(2, t("reminder.modal.description.min", { min: 2 }))
      .max(50, t("reminder.modal.description.max", { max: 50 }))
      .required(t("reminder.modal.description.required")),
    color: Yup.string()
      .matches(/\#[a-zA-Z0-9]+/, t("reminder.modal.color.format"))
      .required(t("reminder.modal.color.required")),
    dateTime: Yup.date().min(new Date(), t("reminder.modal.dateTime.min")).required(t("reminder.modal.dateTime.required")),
    location: Yup.object().required(t("reminder.modal.location.required"))
  })