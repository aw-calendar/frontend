import React from "react"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { Delete } from "@material-ui/icons"
import { Reminder } from "../../types"
import { removeReminder } from "../../store/actions/reminders"
import { Fade, Popper, Theme } from "@material-ui/core"
import { useDispatch } from "react-redux"
import ReminderButton from "../ReminderButton"
import { uniqueId } from "lodash"
import useLocaleMoment from "../../utils/useMoment"

const styled: ThemedStyledInterface<Theme> = styledRef

const Wrapper = styled.div<{
  color: string
  fillBox?: boolean
  width?: string | number | boolean
  height?: string | number | boolean
}>`
  clear: both;
  display: flex;
  font-size: ${(props) => props.theme?.typography.body1.fontSize || "13px"};
  border-radius: 4px;
  line-height: ${(props) => `${props.theme?.spacing(1)}px`};
  background: ${(props) => (props.fillBox ? props.color : "white")};
  border: ${(props) => (props.fillBox ? "0px" : "2px")} solid ${(props) => props.color};
  color: ${(props) => props.theme?.palette.info.dark || "#009aaf"};
  text-decoration: none;
  padding: 4px;
  width: 100%;
  ${(props) => (props.width ? `width: ${props.width};` : "")}
  ${(props) => (props.height ? `height: ${props.height};` : "")}
  justify-content: space-around;
  align-items: center;
`

const Description = styled.span`
  color: #666;
  text-decoration: none;
`

const FlexWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`

const Time = styled.span``

export const ReminderView: React.FunctionComponent<{ reminder: Reminder }> = ({ reminder }) => {
  const dispatch = useDispatch()
  const moment = useLocaleMoment()
  return (
    <Wrapper color={reminder.color} fillBox={false} height="20px" width="100%">
      <img src={reminder?.contidion?.icon} width="30px" height="30px" alt={reminder?.contidion?.text} title={reminder?.contidion?.text} />
      <ReminderButton
        key={uniqueId()}
        state="show-update"
        style={{ width: "100%", height: "100%" }}
        reminder={reminder}
        id="flex-wrapper"
        as={FlexWrapper}
        render={
          <>
            <Description>{reminder.title}</Description>
            <Time>{moment(reminder.dateTime).format("HH:mm")}</Time>
          </>
        }
      />

      <Delete
        onClick={() => dispatch(removeReminder(reminder.id || "none"))}
        style={{ cursor: "pointer" }}
        color="secondary"
      />
    </Wrapper>
  )
}

export const ReminderMini: React.FunctionComponent<{ reminder: Reminder }> = ({ reminder }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget)
  }

  const open = Boolean(anchorEl)
  const id = open ? "transitions-popper" : undefined

  return (
    <span>
      <Wrapper
        onClick={handleClick}
        aria-describedby={id}
        color={reminder.color}
        fillBox={true}
        height="10px"
        width="10px"
      />
      <Popper transition id={id} open={open} anchorEl={anchorEl}>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={100}>
            <span onMouseLeave={handleClick}>
              <ReminderView reminder={reminder} />
            </span>
          </Fade>
        )}
      </Popper>
    </span>
  )
}

const ReminderTip: React.FunctionComponent<{ reminder: Reminder; mini?: boolean }> = ({
  reminder,
  mini = false
}) => {
  if (mini) return <ReminderMini reminder={reminder} />

  return <ReminderView reminder={reminder} />
}

export default ReminderTip
