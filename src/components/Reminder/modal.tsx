import React from "react"
import { Field, Form, Formik, FormikErrors } from "formik"
import { Button, Dialog, DialogContent, FormHelperText, TextField, Theme } from "@material-ui/core"
import styledRef, { ThemedStyledInterface } from "styled-components"
import { Container, Row } from "react-grid-system"
import { Place, Reminder, ReminderDialogOptions } from "../../types"
import LocationPickerField from "../LocationPickerField"
import ColorPickerField from "../ColorPickerField"
import { connect } from "react-redux"
import { AppState } from "../../store"
import { Action, Dispatch } from "redux"
import { addReminderThunk, updateReminderThunk } from "../../store/actions/reminders"
import { useTranslation } from "react-i18next"
import useLocaleMoment from "../../utils/useMoment"
import { INITIAL_REMINDER } from "../../store/reducers/constants"
import { reminderDialog as reminderDialogAction } from "../../store/actions/app"
import { ReminderSchema } from "./schema"

const styled: ThemedStyledInterface<Theme> = styledRef

const StyledRow = styled(Row)`
  padding: ${(props) => `${props.theme?.spacing(1)}px`};
`

const StyledField = styled(Field)`
  min-width: 100%;
`

const firstError = (errors: FormikErrors<Reminder>) => {
  return errors.dateTime || errors.title || errors.description || errors.location || errors.color
}

const ReminderModal: React.FunctionComponent<{
  reminderDialog: ReminderDialogOptions
  dispatch: Dispatch
  date: Date
}> = ({ reminderDialog, dispatch, date }) => {
  const moment = useLocaleMoment()
  const { t } = useTranslation()
  const { state: show } = reminderDialog || {}
  const disabled = show === "show-view"
  const close = () =>
    dispatch(reminderDialogAction({ state: "hide", reminder: INITIAL_REMINDER(date) }))

  const submit = (dataRef: Reminder): void => {
    const data = { ...dataRef, dateTime: new Date(dataRef.dateTime) }

    if (show === "show-create") dispatch(addReminderThunk(data) as unknown as Action<string>)
    else if (show === "show-update") dispatch(updateReminderThunk({ id: data.id || "", params: data }) as unknown as Action<string>)
    close()
  }

  const ref = React.createRef<HTMLDivElement>()

  return (
    <Dialog open={show !== "hide"} onClose={close}>
      <Formik
        initialValues={{ ...reminderDialog?.reminder } as Reminder}
        onSubmit={submit}
        validationSchema={ReminderSchema(t)}
      >
        {({
          isSubmitting,
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          setFieldValue,
          errors
        }) => (
          <DialogContent>
            <Form onSubmit={handleSubmit}>
              <Container>
                <StyledRow justify="center" align="center">
                  <StyledField
                    as={TextField}
                    ref={ref}
                    name="dateTime"
                    label={t("reminder.modal.dateTime.label")}
                    type="datetime-local"
                    value={moment(values.dateTime).format("YYYY-MM-DDTHH:mm:ss")}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </StyledRow>
                <StyledRow justify="center" align="center">
                  <StyledField
                    as={TextField}
                    ref={ref}
                    disabled={disabled}
                    label={t("reminder.modal.title.label")}
                    name="title"
                    type="input"
                    value={values.title || ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </StyledRow>
                <StyledRow justify="center" align="center">
                  <StyledField
                    as={TextField}
                    ref={ref}
                    disabled={disabled}
                    name="description"
                    type="input"
                    label={t("reminder.modal.description.label")}
                    value={values.description || ""}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </StyledRow>
                <StyledRow justify="center" align="center">
                  <LocationPickerField
                    disabled={disabled}
                    value={values.location}
                    label={t("reminder.modal.location.label")}
                    onLocationChange={(val: Place) => setFieldValue("location", val)}
                  />
                </StyledRow>
                <StyledRow justify="center" align="center">
                  <ColorPickerField
                    disabled={disabled}
                    label={t("reminder.modal.color.label")}
                    value={values.color}
                    onChange={(color) => setFieldValue("color", color)}
                  />
                </StyledRow>
                {!disabled && (
                  <StyledRow justify="between" align="center">
                    <Button
                      disabled={isSubmitting}
                      type="submit"
                      variant="contained"
                      color="primary"
                    >
                      {t("reminder.modal.acceptButton.label")}
                    </Button>
                    <Button
                      disabled={isSubmitting}
                      type="button"
                      onClick={close}
                      variant="contained"
                      color="secondary"
                    >
                      {t("reminder.modal.cancelButton.label")}
                    </Button>
                  </StyledRow>
                )}
              </Container>
            </Form>
            {errors && <FormHelperText error>{firstError(errors)}</FormHelperText>}
          </DialogContent>
        )}
      </Formik>
    </Dialog>
  )
}

export default connect(
  (state: AppState) => ({
    reminderDialog: state.appInternal.reminderDialog,
    date: state.appInternal.date.now
  }),
  (dispatch: Dispatch) => ({ dispatch })
)(ReminderModal)
