export interface WeatherCondition {
  text: string
  icon: string
  code: number | string
}

export interface WeatherInfo {
  temp_c: number
  temp_f: number
  is_day: number
  condition: WeatherCondition
  wind_mph: number
  wind_kph: number
  wind_degree: number
  wind_dir: string
  pressure_mb: number
  pressure_in: number
  precip_mm: number
  precip_in: number
  humidity: number
  cloud: number
  feelslike_c: number
  feelslike_f: number
  vis_km: number
  vis_miles: number
  uv: number
  gust_mph: number
  gust_kph: number
}

export interface CurrentWeatherInfo extends WeatherInfo {
  last_updated_epoch: number
  last_updated: string
}

export interface HourWeatherInfo extends WeatherInfo {
  time_epoch: number
  time: string
}

export interface AstroInfo {
  sunrise: string
  sunset: string
  moonrise: string
  moonset: string
  moon_phase: string
  moon_illumination: string
}

export interface ForecastInfo {
  date: string
  date_epoch: number
  day: WeatherInfo
  astro: AstroInfo
  hour: HourWeatherInfo[]
}

export interface WeatherResponse {
  location: {
    name: string
    region: string
    country: string
    lat: number
    lon: number
    tz_id: string
    localtime_epoch: number
    localtime: string
  }
  current: WeatherInfo
  forecast: {
    forecastday: Array<ForecastInfo>
  }
}