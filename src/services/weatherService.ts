import moment from "moment"
import constants from "../config/constants"
import { WeatherCondition, WeatherResponse } from "./types"

const DEFAULT_CONDITION: WeatherCondition = {
  text: "Unknown",
  icon: "//cdn.weatherapi.com/weather/64x64/day/122.png",
  code: 1009
}

export const getWeatherCondition = async (
  { lat, lng }: { lat: number; lng: number },
  date: Date,
  days = 10
): Promise<WeatherCondition | undefined> => {
  console.log("hola")
  const then = moment(new Date()).add(10, "days")
  const desired = moment(date)

  if (desired.isAfter(then)) return DEFAULT_CONDITION

  const { forecast } = await fetch(
    `http://api.weatherapi.com/v1/forecast.json?key=${constants.WEATHER_API_KEY}&q=${lat},${lng}&days=${days}`
  ).then((res) => res.json() as Promise<WeatherResponse>)

  const { forecastday } = forecast

  const hour = forecastday
    .map((fr) => fr.hour)
    .flat()
    .find((h) => moment(h.time, "YYYY-MM-DD HH:mm").startOf("hour").isSame(desired, "hours"))

  return hour?.condition || undefined
}
