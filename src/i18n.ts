import i18n, { InitOptions } from "i18next";
import * as ri18n from "react-i18next";
import detector from "i18next-browser-languagedetector";
import translationEN from "./locales/en.json";
import translationES from "./locales/es.json";

// the translations
export const resources = {
  en: {
    translation: translationEN
  },
  es: {
    translation: translationES
  }
};

export const schema: InitOptions = {
  debug: true,
  resources: { ...resources },
  lng: "en",
  fallbackLng: "en",
  keySeparator: false,
  interpolation: {
    escapeValue: false // react already safes from xss
  }
}

i18n
  .use(detector)
  .use(ri18n.initReactI18next) // passes i18n down to react-i18next
  .init({
   ...schema 
  });

export default i18n;