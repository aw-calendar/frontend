import { AnyAction } from "redux"
import { WeatherCondition } from "./services/types"

export interface Place {
  address: string
  placeId: string
}

export interface Reminder {
  id?: string
  color: string
  title: string
  description: string
  dateTime: Date
  location?: Place
  contidion?: WeatherCondition
}

export interface ReminderQuery {
  from: Date
  to: Date
}

export interface ReminderUpdater {
  id: string
  params: Reminder
}

export type ReminderDialogState = "show-create" | "show-update" | "show-view" | "hide"

export interface ReminderDialogOptions {
  state: ReminderDialogState
  reminder?: Reminder
}

export enum DateComponents {
  day = "day",
  date = "date",
  month = "month",
  year = "year",
  hour = "hour",
}

export interface AppInternalDateOptions {
  now: Date
  currentComponent: DateComponents
}

export type Dispatch = (action: AnyAction) => void
