const constants = {
  GOOGLE_MAPS_API_KEY: "__YOUR_GOOGLE_MAPS_API_KEY__",
  WEATHER_API_KEY: "__YOUR_WEATHERAPI_API_KEY__"
};

export default constants;