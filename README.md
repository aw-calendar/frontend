## Welcome to Planify!

This is a little frontend app made in 5 days intended to manage reminders within a calendar view


## Getting started

As this is a normal React app, you can start the project just by running `npm install` and `npm start` in the terminal, make sure you have [node.js](https://nodejs.org/es/) installed

also, go to `src/config/constants.ts` and set your API keys up!

## What you can do here

It is a calendar view, similar to Google Calendar in essence.

* set the view type on the top right (year, month, date)
* set a specific date on the top center
* add a reminder on the top left
* set the language on the top left
* wherever you see a blue circle with a "+" sign, you can click there to add a reminder
* reminders appear in the caledanr's corresponding date-time as little boxes where you can click to update information about the reminder
* if you click at the trash icon in any reminder box, that reminder will be deleted
* if the date box is too small, the reminder will show itself as a colored box, you can click that colored box to actually see the reminder box

## Stack

This project uses typescript, lint, husky, redux, hooks, functional react components, material UI, styled components, theme provider, i18next, moment.js, google maps API, weatherapi, react-google-places-autocomplete

## Todo
* make the app responsive
* use React.memo to improve performance
* improve the way it uses material theme inside custom styled components
* restructure the project's files and folders a bit
* fix default weather icon
* fix some UI issues related to reminder tooltips and titles
* 